# Running the code and the tests

## Implementation code

Execute the following:

`mvn spring-boot:run`

Add data with the H2 console:

`http://localhost:8080/h2-console`

Execute the following statement in the H2 console:

`INSERT INTO prices (brand_id, start_date, end_date, price_list, product_id, priority, price, currency, last_update, last_update_by) VALUES (1, '2020-06-14T00.00.00', '2020-12-31T23.59.59', 1, 35455, 0, 35.50, 'EUR', '2020-03-26T14.49.07', 'user1')`

Then, use curl in the command line:

```
curl -v "http://localhost:8080/price?productId=35455&brandId=1&priceDate=2020-06-14T10:00:00"
*   Trying ::1...
* TCP_NODELAY set
* Connected to localhost (::1) port 8080 (#0)
> GET /price?productId=35455&brandId=1&priceDate=2020-06-14T10:00:00 HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.64.1
> Accept: */*
>
< HTTP/1.1 200
< Content-Type: application/json
< Transfer-Encoding: chunked
< Date: Sun, 29 Aug 2021 23:10:55 GMT
<
* Connection #0 to host localhost left intact
{"productId":35455,"brandId":1,"priceList":1,"startDate":"2020-06-14T00:00:00","endDate":"2020-12-31T23:59:59","price":{"value":35.50,"currency":"EUR"}}* Closing connection 0
```

## Tests

Execute the following: 

`mvn test`

That will run both unit and integration tests.

## Exercise test

The exercise test is implemented in `src/test/java/com/napptilus/exercise/it/PriceITest.java`.

# Design and libraries

## Design

### Implementation code

Implementation code is distributed in three layers organized in packages: controller, services, entities.

  * controller layer- contains the REST controller `PriceController` and the exception handler `ControllerAdvisor`
     * `PriceController` - handles translation between HTTP/JSON and Java objects and delegates to business service `PriceService`.
     * `ControllerAdvisor` - captures business exception `PriceNotFoundException` and converts it into a HTTP/JSON NotFound response.
  * service layer - contains the business service class `PriceService` and associated classes
     * `PriceService` -  handles the resolution of the price to return based on priority when more than one record is found. A business exception is thron if no record are found.
     * `PriceRepository` - handles database query
     * `PriceResponse` - defines the response to return from the service 
     * `PriceNotFoundException` - business exception
  * entities layer 
     * `Price` - represents a price record in the database
     * `Money` - value class for money

### Tests

Mocks are used in the unit tests `PriceControllerUTest` and `PriceServiceUTest` .

RestTemplate and JUnit 5 test parameterization is utilized in the integration test `PriceITest` which implements the 
exercise test.

Note: Ideally, integration tests should run in the `test-integration` phase and placed in a dedicated project directory which would require additional POM configuration.

## Libraries

The following frameworks, libraries and tools have been used.

  * Spring Boot 2.5.4
  * Spring Data JPA
  * Hibernate 5.4.32
  * H2 Database 1.4
  * Spring Test
  * Mockito
  * JUnit 5
  * Maven 3
  * Java 8

