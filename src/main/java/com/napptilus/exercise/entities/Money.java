package com.napptilus.exercise.entities;

import javax.persistence.Embeddable;
import java.math.BigDecimal;
import java.util.*;

@Embeddable
public class Money {

    private BigDecimal value;

    private String currency;

    private Money(){}

    public Money(BigDecimal value, String currency) {
        Objects.requireNonNull(value);
        Objects.requireNonNull(currency);
        this.value = value;
        this.currency = currency;
    }

    public Money(String value, String currency) {
        this(new BigDecimal(value), currency);
    }

    public BigDecimal getValue() {
        return value;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money money = (Money) o;
        return Objects.equals(value, money.value) && Objects.equals(currency, money.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, currency);
    }

    @Override
    public String toString() {
        return "Money{" +
                "value=" + value +
                ", currency='" + currency + '\'' +
                '}';
    }
}
