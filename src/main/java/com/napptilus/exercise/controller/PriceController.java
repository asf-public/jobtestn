package com.napptilus.exercise.controller;

import com.napptilus.exercise.service.PriceNotFoundException;
import com.napptilus.exercise.service.PriceResponse;
import com.napptilus.exercise.service.PriceService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping(path = "/price", produces = "application/json")
public class PriceController {

    private final PriceService priceService;

    public PriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @GetMapping
    public PriceResponse findByDateBrandAndProductId(
            @RequestParam("priceDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime priceDate,
            @RequestParam("brandId") Long brandId,
            @RequestParam("productId") Long productId) throws PriceNotFoundException {

        return priceService.findByDateBrandAndProductId(priceDate, brandId, productId);

    }
}
