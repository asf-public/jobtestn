package com.napptilus.exercise.service;

import com.napptilus.exercise.entities.Price;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class PriceService {

    @Autowired
    private PriceRepository priceRepository;

    /**
     * Find price according to date, brand and product. Return the one with the highest priority if more than one found. Throw exception if none found.
     *
     * @param priceDate
     * @param brandId
     * @param productId
     * @return Applicable price.
     * @throws PriceNotFoundException
     */
    public PriceResponse findByDateBrandAndProductId(
            LocalDateTime priceDate,
            Long brandId,
            Long productId) throws PriceNotFoundException {

        // Retrieve price list.
        final List<Price> priceList = priceRepository.findByDateBrandAndProductId(priceDate, brandId, productId);

        return priceList.stream()
                // Sort by highest priority.
                .max(Comparator.comparing(Price::getPriority))
                // Convert Price to PriceResponse.
                .map(price -> new PriceResponse(price.getProductId(), price.getBrandId(), price.getPriceList(), price.getStartDate(), price.getEndDate(), price.getPrice()))
                // Throw exception if empty list.
                .orElseThrow(PriceNotFoundException::new);
    }
}
