package com.napptilus.exercise.service;

import com.napptilus.exercise.entities.Money;

import java.time.LocalDateTime;

public class PriceResponse {

    private final Long productId;

    private final Long brandId;

    private final Long priceList;

    private final LocalDateTime startDate;

    private final LocalDateTime endDate;

    private final Money price;

    public PriceResponse(Long productId, Long brandId, Long priceList, LocalDateTime startDate, LocalDateTime endDate, Money price) {
        this.productId = productId;
        this.brandId = brandId;
        this.priceList = priceList;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
    }

    public Long getProductId() {
        return productId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public Long getPriceList() {
        return priceList;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public Money getPrice() {
        return price;
    }
}
