package com.napptilus.exercise.service;

import com.napptilus.exercise.entities.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

interface PriceRepository extends JpaRepository<Price, Long> {
    @Query("select p from Price p where p.startDate <= ?1 and p.endDate >= ?1 and p.brandId = ?2 and p.productId = ?3 order by p.priority desc")
    List<Price> findByDateBrandAndProductId(LocalDateTime priceDate,
                                            Long brandId,
                                            Long productId);
}
