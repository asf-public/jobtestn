package com.napptilus.exercise.controller;

import com.napptilus.exercise.entities.Money;
import com.napptilus.exercise.service.PriceNotFoundException;
import com.napptilus.exercise.service.PriceResponse;
import com.napptilus.exercise.service.PriceService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.autoconfigure.data.jpa.*;
import org.springframework.boot.autoconfigure.orm.jpa.*;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Unit test for Price controller.
 */
@SpringBootTest
@AutoConfigureMockMvc
@EnableAutoConfiguration(exclude= {JpaRepositoriesAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class PriceControllerUTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PriceService priceService;

    @Test
    public void whenAPriceExistsThenItIsRetrievedAsExpected() throws Exception {

        final String requestedDate = "2020-06-14T10:00:00";
        final Long requestedBrandId = 1L;
        final Long requestedProductId = 35455L;

        final Long returnedPriceListId = 1L;
        final String returnedStartDate = "2020-06-14T10:00:00";
        final String returnedEndDate = "2020-06-24T00:00:00";
        final Money returnedPrice = new Money("34.5", "EUR");

        when(priceService.findByDateBrandAndProductId(LocalDateTime.parse(requestedDate), requestedBrandId, requestedProductId))
                .thenReturn(new PriceResponse(
                        requestedProductId,
                        requestedBrandId,
                        returnedPriceListId,
                        LocalDateTime.parse(returnedStartDate),
                        LocalDateTime.parse(returnedEndDate),
                        returnedPrice));

        this.mockMvc.perform(get("/price")
                        .param("priceDate", requestedDate)
                        .param("brandId", requestedBrandId.toString())
                        .param("productId", requestedProductId.toString())).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.productId").value(requestedProductId))
                .andExpect(jsonPath("$.brandId").value(requestedBrandId))
                .andExpect(jsonPath("$.priceList").value(returnedPriceListId))
                .andExpect(jsonPath("$.startDate").value(returnedStartDate))
                .andExpect(jsonPath("$.endDate").value(returnedEndDate))
                .andExpect(jsonPath("$.price.value").value(returnedPrice.getValue()))
                .andExpect(jsonPath("$.price.currency").value(returnedPrice.getCurrency()));
    }

    @Test
    public void whenAPriceDoesNotExistThenNotFoundIsExpected() throws Exception {

        final String requestedDate = "2020-06-14T10:00:00";
        final Long requestedBrandId = 1L;
        final Long requestedProductId = 35455L;

        when(priceService.findByDateBrandAndProductId(LocalDateTime.parse(requestedDate), requestedBrandId, requestedProductId))
                .thenThrow(new PriceNotFoundException());

        this.mockMvc.perform(get("/price")
                        .param("priceDate", requestedDate)
                        .param("brandId", requestedBrandId.toString())
                        .param("productId", requestedProductId.toString())).andDo(print()).andExpect(status().isNotFound());
    }
}
