package com.napptilus.exercise.it;

import com.napptilus.exercise.entities.*;
import com.napptilus.exercise.service.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.web.client.*;
import org.springframework.boot.web.server.*;
import org.springframework.http.*;
import org.springframework.test.context.jdbc.*;

import java.time.*;
import java.time.format.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Integration test for the Price endpoint.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PriceITest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Exercise integration test.
     * See also {@link com.napptilus.exercise.controller.PriceControllerUTest}, {@link PriceServiceUTest}, {@link com.napptilus.exercise.entity.MoneyUTest}.
     * <pre>
     * Desarrollar unos test al endpoint rest que validen las siguientes peticiones al servicio con los datos del ejemplo:
     * ○ Test 1: petición a las 10:00 del día 14 del producto 35455 (ZARA)
     * ○ Test 2: petición a las 16:00 del día 14 del producto 35455 (ZARA)
     * ○ Test 3: petición a las 21:00 del día 14 del producto 35455 (ZARA)
     * ○ Test 4: petición a las 10:00 del día 15 del producto 35455 (ZARA)
     * ○ Test 5: petición a las 21:00 del día 16 del producto 35455 (ZARA)
     * </pre>
     */
    @Sql(statements = {
            "INSERT INTO prices (brand_id, start_date, end_date, price_list, product_id, priority, price, currency, last_update, last_update_by) " +
                    "VALUES (1, '2020-06-14T00.00.00', '2020-12-31T23.59.59', 1, 35455, 0, 35.50, 'EUR', '2020-03-26T14.49.07', 'user1')",
            "INSERT INTO prices (brand_id, start_date, end_date, price_list, product_id, priority, price, currency, last_update, last_update_by) " +
                    "VALUES (1, '2020-06-14T15.00.00', '2020-06-14T18.30.00', 2, 35455, 1, 25.45, 'EUR', '2020-05-26T15.38.22', 'user1')",
            "INSERT INTO prices (brand_id, start_date, end_date, price_list, product_id, priority, price, currency, last_update, last_update_by) " +
                    "VALUES (1, '2020-06-15T00.00.00', '2020-06-15T11.00.00', 3, 35455, 1, 30.50, 'EUR', '2020-05-26T15.39.22', 'user1')",
            "INSERT INTO prices (brand_id, start_date, end_date, price_list, product_id, priority, price, currency, last_update, last_update_by) " +
                    "VALUES (1, '2020-06-15T16.00.00', '2020-12-31T23.59.59', 4, 35455, 1, 38.95, 'EUR', '2020-06-02T10.14.00', 'user1')"},
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(statements = "DELETE FROM prices",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @ParameterizedTest
    @CsvSource(value = {
            "10:00;14;35455;1;2020-06-14T00:00:00;2020-12-31T23:59:59;1;35.50;EUR",
            "16:00;14;35455;1;2020-06-14T15:00:00;2020-06-14T18:30:00;2;25.45;EUR",
            "21:00;14;35455;1;2020-06-14T00:00:00;2020-12-31T23:59:59;1;35.50;EUR",
            "10:00;15;35455;1;2020-06-15T00:00:00;2020-06-15T11:00:00;3;30.50;EUR",
            "21:00;16;35455;1;2020-06-15T16:00:00;2020-12-31T23:59:59;4;38.95;EUR"}, delimiter = ';')
    public void whenEndpointIsQueriedForExistingPrice_theResponseIsAsExpected(
            String hour, String day, String productId, String brandId,
            String expectedStartDate, String expectedEndDate, Long expectedPriceList, String expectedPriceValue, String expectedCurrency) {

        // Build URL (Note: StringBuilder wouldn't provide much performance enhancement here.)
        String url = "http://localhost:" + port +
                "/price?priceDate=2020-06-" +
                day +
                "T" +
                hour +
                ":00&brandId=" +
                brandId +
                "&productId=" +
                productId;

        // Call REST service.
        final ResponseEntity<PriceResponse> responseEntity = this.restTemplate.getForEntity(url,
                PriceResponse.class);

        // Assertions
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        final PriceResponse priceResponse = responseEntity.getBody();
        assertNotNull(priceResponse);
        assertNotNull(priceResponse.getProductId());
        assertEquals(35455L, priceResponse.getProductId());
        assertNotNull(priceResponse.getBrandId());
        assertEquals(1L, priceResponse.getBrandId());
        assertNotNull(priceResponse.getPriceList());
        assertEquals(expectedPriceList, priceResponse.getPriceList());
        assertNotNull(priceResponse.getStartDate());
        assertEquals(LocalDateTime.parse(expectedStartDate, DateTimeFormatter.ISO_LOCAL_DATE_TIME), priceResponse.getStartDate());
        assertNotNull(priceResponse.getEndDate());
        assertEquals(LocalDateTime.parse(expectedEndDate, DateTimeFormatter.ISO_LOCAL_DATE_TIME), priceResponse.getEndDate());
        assertNotNull(priceResponse.getPrice());
        // Asserts price has been resolved correctly.
        assertEquals(new Money(expectedPriceValue, expectedCurrency), priceResponse.getPrice());
    }

    @Test
    public void whenEndpointIsQueriedForNonExistingPrice_theResponseIsNotFound() {

        // Build URL
        final StringBuilder urlBuilder = new StringBuilder("http://localhost:");
        urlBuilder.append(port);
        urlBuilder.append("/price?priceDate=1970-06-01T00:00:00&brandId=0&productId=0");

        // Call REST service.
        final ResponseEntity<PriceResponse> responseEntity = this.restTemplate.getForEntity(urlBuilder.toString(),
                PriceResponse.class);

        // Assertions
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }
}
