package com.napptilus.exercise.entity;

import com.napptilus.exercise.entities.*;
import org.junit.jupiter.api.*;

import java.math.*;

import static org.junit.jupiter.api.Assertions.*;

public class MoneyUTest {

    @Test
    public void testEquals() {
        assertEquals(new Money("34.5", "EUR"), new Money("34.5", "EUR"));
        assertEquals(new Money("34.5", "EUR"), new Money(new BigDecimal("34.5"), "EUR"));
    }
}
