package com.napptilus.exercise.service;

import com.napptilus.exercise.entities.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.autoconfigure.data.jpa.*;
import org.springframework.boot.autoconfigure.orm.jpa.*;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.*;

import java.time.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Unit test for Price service.
 */
@SpringBootTest
@EnableAutoConfiguration(exclude= {JpaRepositoriesAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class PriceServiceUTest {
    @Autowired
    private PriceService priceService;

    @MockBean
    private PriceRepository mockedPriceRepository;

    @Test
    public void whenAPriceExists_thenItIsRetrievedAsExpected() throws Exception {

        final String requestedDate = "2020-06-14T10:00:00";
        final Long requestedBrandId = 1L;
        final Long requestedProductId = 35455L;

        final Long returnedPriceListId = 1L;
        final Integer returnedPriority = 1;
        final String returnedStartDate = "2020-06-14T10:00:00";
        final String returnedEndDate = "2020-06-24T00:00:00";
        final Money returnedPrice = new Money("34.5", "EUR");

        final Price mockedPrice = mockedPrice(requestedBrandId, requestedProductId, returnedPriceListId, returnedPriority, returnedStartDate, returnedEndDate, returnedPrice);
        when(mockedPriceRepository.findByDateBrandAndProductId(LocalDateTime.parse(requestedDate), requestedBrandId, requestedProductId))
                .thenReturn(Collections.singletonList(mockedPrice));

        final PriceResponse response = priceService.findByDateBrandAndProductId(LocalDateTime.parse(requestedDate), requestedBrandId, requestedProductId);
        assertEquals(requestedBrandId, response.getBrandId());
        assertEquals(returnedPriceListId, response.getPriceList());
        assertEquals(returnedPrice, response.getPrice());
        assertEquals(requestedProductId, response.getProductId());
        assertEquals(LocalDateTime.parse(returnedStartDate), response.getStartDate());
        assertEquals(LocalDateTime.parse(returnedEndDate), response.getEndDate());
    }

    @Test
    public void whenSeveralPricesExist_thenTheOneWithHighestPriorityIsReturned() throws Exception {

        final String requestedDate = "2020-06-14T10:00:00";
        final Long requestedBrandId = 1L;
        final Long requestedProductId = 35455L;

        final Long returnedPriceListId = 1L;
        final String returnedStartDate = "2020-06-14T10:00:00";
        final String returnedEndDate = "2020-06-24T00:00:00";

        final Integer lowerPriority = 0;
        final Money lowerPriorityPrice = new Money("34.5", "EUR");
        final Integer higherPriority = 1;
        final Money higherPriorityPrice = new Money("55.5", "EUR");

        final Price mockedLowerPrice = mockedPrice(requestedBrandId, requestedProductId, returnedPriceListId, lowerPriority, returnedStartDate, returnedEndDate, lowerPriorityPrice);
        final Price mockedHigherPrice = mockedPrice(requestedBrandId, requestedProductId, returnedPriceListId, higherPriority, returnedStartDate, returnedEndDate, higherPriorityPrice);


        when(mockedPriceRepository.findByDateBrandAndProductId(LocalDateTime.parse(requestedDate), requestedBrandId, requestedProductId))
                .thenReturn(Arrays.asList(mockedLowerPrice, mockedHigherPrice));

        final PriceResponse response = priceService.findByDateBrandAndProductId(LocalDateTime.parse(requestedDate), requestedBrandId, requestedProductId);
        assertEquals(requestedBrandId, response.getBrandId());
        assertEquals(returnedPriceListId, response.getPriceList());
        assertEquals(higherPriorityPrice, response.getPrice());
        assertEquals(requestedProductId, response.getProductId());
        assertEquals(LocalDateTime.parse(returnedStartDate), response.getStartDate());
        assertEquals(LocalDateTime.parse(returnedEndDate), response.getEndDate());
    }

    @Test
    public void whenAPriceDoesNotExist_thenNotFoundIsExpected() {

        final String requestedDate = "2020-06-14T10:00:00";
        final Long requestedBrandId = 1L;
        final Long requestedProductId = 35455L;

        when(mockedPriceRepository.findByDateBrandAndProductId(LocalDateTime.parse(requestedDate), requestedBrandId, requestedProductId))
                .thenReturn(Collections.emptyList());

        assertThrows(PriceNotFoundException.class, () -> priceService.findByDateBrandAndProductId(LocalDateTime.parse(requestedDate), requestedBrandId, requestedProductId));

    }

    private Price mockedPrice(Long requestedBrandId, Long requestedProductId, Long returnedPriceListId, Integer returnedPriority, String returnedStartDate, String returnedEndDate, Money returnedPrice) {
        final Price mockedPrice = mock(Price.class);
        when(mockedPrice.getBrandId()).thenReturn(requestedBrandId);
        when(mockedPrice.getProductId()).thenReturn(requestedProductId);
        when(mockedPrice.getPriceList()).thenReturn(returnedPriceListId);
        when(mockedPrice.getPriority()).thenReturn(returnedPriority);
        when(mockedPrice.getStartDate()).thenReturn(LocalDateTime.parse(returnedStartDate));
        when(mockedPrice.getEndDate()).thenReturn(LocalDateTime.parse(returnedEndDate));
        when(mockedPrice.getPrice()).thenReturn(returnedPrice);
        return mockedPrice;
    }

}
